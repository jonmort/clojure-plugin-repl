package it;


import clojure.lang.RT;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class ClojureReplTest {

    @Test
    public void testConnectRepl() throws IOException {
        RT.loadResourceScript("jonmort/plugin/repl/test.clj");
        assertTrue((Boolean)RT.var("jonmort.plugin.repl.test", "test-connect-repl").invoke());
    }
}
