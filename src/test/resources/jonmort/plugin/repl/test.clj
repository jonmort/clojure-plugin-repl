(ns jonmort.plugin.repl.test
  (:use clojure.test)
  (:require [clojure.tools.nrepl :as repl]))

(defn connect-repl []
  (with-open [conn (repl/connect :port 7888)]
    (-> (repl/client conn 8000)    ; message receive timeout required
      (repl/message {:op "eval" :code "(+ 2 3)"})
      repl/response-values)))

(defn test-connect-repl []
  (is (= [5]
    (connect-repl))))
