(ns jonmort.plugin.repl.server
  (:gen-class :implements[org.springframework.beans.factory.DisposableBean
                          org.springframework.beans.factory.InitializingBean])
  (:use [clojure.tools.nrepl.server :only [start-server stop-server]]))

(def server (atom nil))

(defn -destroy [_]
  (if-not (nil? @server)
    (stop-server @server)
    (reset! server nil)))

(defn -afterPropertiesSet [_]
    (let [s (start-server :port 7888)]
      (reset! server s)))
