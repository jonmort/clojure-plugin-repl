(ns jonmort.plugin.repl.osgi
  (:gen-class :main false)
  (:import (com.atlassian.sal.api.transaction TransactionTemplate TransactionCallback)))

(defn bundle-context []
  (-> (org.osgi.framework.FrameworkUtil/getBundle jonmort.plugin.repl.osgi) .getBundleContext))

(defn service-reference [name]
  (-> (bundle-context) (.getServiceReference name)))

(defn service [name]
  (-> (bundle-context) (.getService (service-reference name))))

(defmacro with-transaction
  "Execute the provided code in a transation"
  [& body]
  `(let [trans-temp# (service "com.atlassian.sal.api.transaction.TransactionTemplate")
         handler# (proxy [TransactionCallback] []
                   (doInTransaction [] ~@body))]
     (.execute trans-temp# handler#)))

(comment
  (.getBundles (bundle-context))
  (.getServiceReference (bundle-context) "com.atlassian.applinks.api.EntityLinkService")
  (service "com.atlassian.applinks.api.EntityLinkService")
  (def app-links (service "com.atlassian.applinks.api.ApplicationLinkService"))
  (def entity-links (service "com.atlassian.applinks.api.EntityLinkService"))

  (let [app-props (service "com.atlassian.sal.api.ApplicationProperties")]
    {:version (.getVersion app-props)
     :home (.getHomeDirectory app-props)
     :base-url (.getBaseUrl app-props)})

  (def trans-temp (service "com.atlassian.sal.api.transaction.TransactionTemplate"))
  (def space-mgr (service "com.atlassian.confluence.spaces.SpaceManager"))
  (.execute trans-temp (proxy [TransactionCallback] []
                         (doInTransaction [] (.getAllSpaces space-mgr))))

  (macroexpand `(with-transaction (first (.getAllSpaces space-mgr))))
  (with-transaction (pr-str (bean (first (.getAllSpaces space-mgr)))))
  )
